from scenario_main import prepare_simulation_data, calculate_scenario
from commons import Methods


def main():
    prepare_simulation_data(overwrite=False)

    for method in Methods:
        calculate_scenario(method, config="run", scenario="detail", EV="old")
        calculate_scenario(method, config="run", scenario="detail", EV="new")


if __name__ == '__main__':
    main()

'''
Created on 24.05.2022

@author: Fernando Penaherrera @UOL/OFFIS
'''
import math
from os.path import join
import os
import warnings
import pandas as pd
import pandapower as pp
from pvlib._deprecation import pvlibDeprecationWarning
from pvlib.location import Location
from pvlib.modelchain import ModelChain
from pvlib.pvsystem import PVSystem as PVLibSystem
from pvlib.temperature import TEMPERATURE_MODEL_PARAMETERS
from pvlib.irradiance import erbs
from pandas.core.common import SettingWithCopyWarning
from os import walk
import random
import gzip
import pickle
import logging

from commons import DATA_PROCESSED_DIR, DATA_RAW_DIR, DATA_CAR_DIR

logging.basicConfig(level=logging.INFO)
logging.basicConfig(format='%(asctime)s %(message)s')
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=pvlibDeprecationWarning)
warnings.filterwarnings("ignore", category=SettingWithCopyWarning)
warnings.filterwarnings("ignore", category=pd.errors.PerformanceWarning)

GRID_DATA_FILE = join(DATA_RAW_DIR, "grid_topology.xlsx")


def create_household_data(
        houses=65,
        units_per_house=6,
        year=2019):
    """Compose a CSV file with information on the loads

    Args:
        houses (int, optional): Number of houses. Defaults to 65.
        units_per_house (int, optional): Number of units (apartments) per house. Defaults to 6.
        year (int, optional): Year of the timeseries. Defaults to 2019.

    Returns:
        string: Path of the created data
    """
    raw_filename = "Gesamtverbrauchsdaten.csv"
    # Read csv file as dataframe

    raw_data_path = join(DATA_RAW_DIR, raw_filename)

    df = pd.read_csv(raw_data_path,
                     sep=";",
                     parse_dates=['rec_time'])

    # Delete the column "Einheit"
    del df['Einheit']

    # Time increment
    START = f"{year}-01-01 00:00:00"
    END = f"{year}-12-31 23:45:00"
    dti = pd.date_range(start=START, end=END, freq="15T")

    steps = len(dti)
    delta = df.iloc[1, 0] - df.iloc[0, 0]
    delta_t = delta.total_seconds() / 3600  # seconds to hour
    df.set_index('rec_time', inplace=True)

    # Convert kWh to W, scaling to the time resolution, from
    df_w = df / (delta_t) * 1000
    rows_drop = df_w.shape[0] - steps  # Number of rows to drop

    # limiting the data to one year
    # Dropping last n rows using drop
    df_w.drop(df_w.tail(rows_drop).index, inplace=True)

    df_w["Time"] = dti
    df_w.set_index('Time', inplace=True)

    # Creating random columns: create a dataframe with all Households in
    # Oelper Berge (65*6 HH = 390 HH) by selecting random columns in df_w
    n_samples = houses * units_per_house
    hh_all = df_w.sample(
        n_samples,
        axis='columns',
        replace=True,
        random_state=123456)

    # new header is also random, therefore remove header to avoid
    # misunderstanding
    hh_all.columns = range(hh_all.shape[1])

    # Group and sum every 6 columns
    hh_all_grouped = hh_all.groupby(
        [[math.floor(i / units_per_house) for i in range(0, n_samples)]], axis=1).sum()

    # Rename headers
    hh_all_grouped.columns = [f"House{i}" for i in range(0, houses)]
    hh_all_grouped.index.rename("Time", inplace=True)

    # Save in CSV format
    csv_filename = join(DATA_PROCESSED_DIR, "Household_Loads.csv")
    hh_all_grouped.to_csv(csv_filename)
    logging.info(f"Household data saved to {csv_filename}")
    return "Household_Loads.csv"


def create_pv_sizing(pv_excel_data: str, pv_scaling=1):
    """reates a dictionary with the parameters of the PV

    Args:
        pv_excel_data (str): Filename with the information on PV Sizing and system configuration
        pv_scaling (int, optional): Scaling of the PV (for experimentation). Defaults to 1.

    Returns:
        str: Dataframe with the sorted data.
    """
    pv_data = pd.read_excel(pv_excel_data, "dict",
                            index_col="bus", skiprows=range(1, 2))
    pv_data = pv_data.sort_index()

    if pv_scaling:
        pv_data['p_ac'] *= pv_scaling
        pv_data['p_dc'] *= pv_scaling

    pv_data = pv_data[['albedo',
                       'elevation',
                       'inclination',
                       'latitude',
                       'longitude',
                       'surface_azimuth',
                       'timezone',
                       'p_ac',
                       'p_dc',
                       'inverter_eff']]

    # It is not necesarry to save to json, but I leave this here
    #pv_json_dir = "data/pv_sizing.json"
    #pv_data.to_json(pv_json_dir, orient="index")
    return pv_data


def create_pv_csv_data(pv_data, meteo_data_path=None):
    """Creates a CSV with the timeseries of PV Production

    Args:
        pv_data (pd.Dataframe): A dataframe with the description of the PV System
        meteo_data_path (str, optional): Path to the Meteo Data file. Defaults to None.

    Returns:
        str: Path of the created CSV Timeseries
    """
    # Get a weather Dataframe
    if meteo_data_path is None:
        pv_filename = "Braunschweig_meteodata_2019_15min.csv"
        meteo_data_path = join(DATA_PROCESSED_DIR, pv_filename)

    weather = pd.read_csv(
        meteo_data_path,
        parse_dates=True,
        index_col="Time")

    # Create a dataframe for future saving
    pv_results = pd.DataFrame(index=weather.index)

    # Calculate pv power
    for key, val in pv_data.iterrows():
        val["temp_model_params"] = TEMPERATURE_MODEL_PARAMETERS['sapm']['open_rack_glass_polymer']

        location = Location(latitude=val["latitude"],
                            longitude=val["longitude"],
                            altitude=val["elevation"])

        zenith = location.get_solarposition(
            times=weather.index, temperature=weather["AirTemperature"])
        missing_radiation_pars = erbs(weather["GlobalRadiation"],
                                      zenith['zenith'],
                                      weather.index)
        dhi = missing_radiation_pars['dhi']
        dni = missing_radiation_pars['dni']
        weather_df = weather[["GlobalRadiation",
                              "AirTemperature", "WindSpeed"]]
        weather_df["dni"] = dni
        weather_df["dhi"] = dhi

        # PV Watts requires specific column names
        weather_df.rename(columns={
            'GlobalRadiation': 'ghi',
            'AirTemperature': 'temp_air',
            'WindSpeed': 'wind_speed'},
            inplace=True)

        # Create a PVSystem based on the configuration
        module_params = dict(
            pdc0=val["p_dc"], gamma_pdc=-0.003)

        p_ac = val["p_ac"]
        inverter_eff = val["inverter_eff"]
        inverter_params = dict(pdc0=p_ac / inverter_eff)

        system = PVLibSystem(
            surface_tilt=val["inclination"],
            surface_azimuth=val["surface_azimuth"],
            module_parameters=module_params,
            inverter_parameters=inverter_params,
            temperature_model_parameters=val["temp_model_params"])

        model_chain = ModelChain.with_pvwatts(
            system=system, location=location)

        model_chain.run_model(weather_df)
        pv_results[f"PV{key}"] = model_chain.results.ac

    pv_res_filename = join(DATA_PROCESSED_DIR, "pv_production.csv")
    pv_results.to_csv(pv_res_filename)
    logging.info(f"PV Productionb data saved to {pv_res_filename}")

    return pv_results


def create_pv_data(meteo_data_path):
    """Chaining the two functions to a single pipeline

    Args:
        meteo_data_path (string): Path to the Meteo Data File

    Returns:
        str: Path of the created CSV Timeseries
    """
    pv_excel_data = join(DATA_RAW_DIR, "PV_Sizing.xlsx")
    pv_data = create_pv_sizing(pv_excel_data, pv_scaling=None)

    if meteo_data_path is None:
        meteo_data_path = join(
            DATA_PROCESSED_DIR, "Braunschweig_meteodata_2019_10min.csv")
    pv_results = create_pv_csv_data(pv_data, meteo_data_path)

    return pv_results


def create_grid(grid_file_name=GRID_DATA_FILE):
    """Creates a grid for a district with the information in the Excel file
    Saves to a json file in the current folder

    Args:
        grid_file_name (str, optional): Excel file name with the topology of the grid. Defaults to commons.GRID_DATA_FILE.

    Returns:
        pandapower.net: A pandapower net object with the grid.
    """

    # Create empty grid
    net = pp.create_empty_network()

    # Create Buses
    df_bus = pd.read_excel(grid_file_name,
                           sheet_name="bus", index_col=0)
    for idx in df_bus.index:
        pp.create_bus(net, vn_kv=df_bus.at[idx, "vn_kv"],
                      name=df_bus.at[idx, "name"],
                      geodata=((df_bus.at[idx, "x"]), (df_bus.at[idx, "y"])),
                      type=df_bus.at[idx, "type"],
                      in_service=df_bus.at[idx, "in_service"])

    # Create loads
    df_load = pd.read_excel(grid_file_name,
                            sheet_name="load", index_col=0)
    for idx in df_load.index:
        pp.create_load(net, bus=df_load.at[idx, "bus"],
                       p_mw=df_load.at[idx, "p_mw"],
                       name=df_load.at[idx, "name"],
                       scaling=df_load.at[idx, "scaling"])

    # Create external grids
    df_ext_grid = pd.read_excel(
        grid_file_name,
        sheet_name="externalgrid", index_col=0)
    for idx in df_ext_grid.index:
        pp.create_ext_grid(net, bus=df_ext_grid.at[idx, "bus"],
                           vm_pu=df_ext_grid.at[idx, "vm_pu"],
                           name=df_ext_grid.at[idx, "name"])

    # CREATE NON-STANDARD LINES
    df_line = pd.read_excel(grid_file_name, sheet_name="line", index_col=0)

    for idx in df_line.index:
        pp.create_line_from_parameters(net, name=df_line.at[idx, "name"],
                                       from_bus=df_line.at[idx, "from_bus"],
                                       to_bus=df_line.at[idx, "to_bus"],
                                       length_km=df_line.at[idx, "length_km"],
                                       r_ohm_per_km=df_line.at[idx,
                                                               "r_ohm_per_km"],
                                       x_ohm_per_km=df_line.at[idx,
                                                               "x_ohm_per_km"],
                                       c_nf_per_km=df_line.at[idx,
                                                              "c_nf_per_km"],
                                       g_us_per_km=df_line.at[idx,
                                                              "g_us_per_km"],
                                       max_i_ka=df_line.at[idx, "max_i_ka"])

    # CREATE STANDARD TRAFOS
    df_trafo = pd.read_excel(grid_file_name,
                             sheet_name="trafo",
                             index_col=0)
    for idx in df_trafo.index:
        pp.create_transformer(net, hv_bus=df_trafo.at[idx, "hv_bus"],
                              lv_bus=df_trafo.at[idx, "lv_bus"],
                              std_type=df_trafo.at[idx, "std_type"])

    # CREATE SGENS as PV
    df_pv = pd.read_excel(grid_file_name,
                          sheet_name="sgen",
                          index_col=0)
    for idx in df_pv.index:
        pp.create_sgen(net,
                       bus=df_pv.at[idx, "bus"],
                       p_mw=df_pv.at[idx, "p_mw"],
                       q_mvar=df_pv.at[idx, "q_mvar"],
                       name=df_pv.at[idx, "name"],
                       scaling=df_pv.at[idx, "scaling"],
                       type=df_pv.at[idx, "type"],
                       in_service=True
                       )

    # CREATE STORAGE as
    df_sto = pd.read_excel(grid_file_name,
                           sheet_name="storage",
                           index_col=0)

    # Momentary real power of the storage
    # (positive for charging, negative for discharging)
    for idx in df_sto.index:
        pp.create_storage(net,
                          bus=df_sto.at[idx, "bus"],
                          p_mw=df_sto.at[idx, "p_mw"],
                          q_mvar=df_sto.at[idx, "q_mvar"],
                          name=df_sto.at[idx, "name"],
                          scaling=df_sto.at[idx, "scaling"],
                          max_e_mwh=df_sto.at[idx, "max_e_mwh"],
                          in_service=True
                          )

    # EVs are modelled as extra loads.
    # Positive for grid consumption/EV charging
    # Negative for grid insertion/EV discharging
    df_ev = pd.read_excel(grid_file_name,
                          sheet_name="ev",
                          index_col=0)
    for idx in df_ev.index:
        # we are aming at around 144 cars because there are 144 available parking spaces
        # So I create 3 extra loads per bus
        for i in range(0, 3):
            pp.create_load(net,
                           bus=df_ev.at[idx, "bus"],
                           p_mw=df_ev.at[idx, "p_mw"],
                           name=df_ev.at[idx, "name"] + f"_{i}",
                           scaling=df_ev.at[idx, "scaling"],
                           in_service=True
                           )

    grid_name = join(DATA_PROCESSED_DIR, "Grid_Model.json")
    pp.to_json(net, grid_name)

    logging.info(f"Grid file saved to {grid_name}")

    return net


def resample_data(filename: str, path=None, mins=10):
    """Resamples the data from 1h to 15 mins resolution
    Args:
        filename (str): Original file to be resampled. Must be a CSV with timeseries
        path (str, optional): Path of the file. Defaults to None.
        mins (int, optional): Minutes for resampling. Defaults to 10.

    Returns:
        str: Path of the resampled timeseries.
    """

    if path is None:
        path = DATA_RAW_DIR
    full_filename = join(path, filename)
    # Some files start at 0, others at 1
    try:
        df = pd.read_csv(full_filename, skiprows=1,
                         index_col="Time", parse_dates=True)
    except:
        df = pd.read_csv(full_filename, skiprows=0,
                         index_col="Time", parse_dates=True)
    df_resampled = pd.DataFrame()

    for c in df.columns:
        series = df[c]

        series_down = series.resample(f"{mins}min")
        series_int = series_down.interpolate(method='linear')  # "spline"
        df_resampled[c] = series_int

    new_filename = filename[0:-4] + f"_{mins}min.csv"
    new_filename_full = join(DATA_PROCESSED_DIR, new_filename)
    df_resampled.to_csv(new_filename_full)

    logging.info(f"Data resampled to {new_filename_full}")
    return new_filename_full

def create_car_data():
    """Creates time series of EVs demand at the charging stations

    Returns:
        str: Path of the created CSV with the timeseries
    """
    car_list = []
    for (_, _, filenames) in walk(DATA_CAR_DIR):
        car_list.extend(filenames)
        break

    car_list = [car for car in car_list if car.endswith('.pickle') == True]
    car_list = car_list[0:144]
    car_list.extend(["empty", "empty", "empty", ])
    random.seed(123456)

    # create a dict and randomly sort the dict
    random.shuffle(car_list)
    assert(len(car_list) == 49*3)
    data_car_kw = pd.DataFrame()
    for i in range(0, len(car_list)):
        if car_list[i] == "empty":
            data_car_kw[f"car_{i}"] = 0
        else:
            car_filename = join(DATA_CAR_DIR, car_list[i])
            pickle_off = gzip.open(car_filename, "rb")
            data = pickle.load(pickle_off)
            pickle_off.close()
            charg_point = data["profile"]["charging_point"]
            charg_cap = data["profile"]["charging_cap"]
            charg_soc = data["profile"]["soc"]
            total_load = [0]*len(charg_cap)
            # if charging point is home and soc <1, then load with charging_cap
            for x in range(0, len(charg_cap)):
                if charg_point[x] == "home" and charg_soc[x] < 1:
                    total_load[x] = charg_cap[x]
            data_car_kw[f"car_{i}"] = total_load  # charging

    START = "2019-01-01 00:00:00"
    END = "2019-12-31 23:45:00"
    dti = pd.date_range(start=START, end=END, freq="15T")
    steps = len(dti)
    data_car_kw = data_car_kw.head(steps)
    data_car_kw["Time"] = dti
    data_car_kw.set_index("Time", inplace=True)
    data_car_w = data_car_kw*1000 #kW to W
    filename = join(DATA_PROCESSED_DIR, "ev_loads.csv")
    data_car_w.to_csv(filename)
    logging.info(f"EV data saved to {filename}")
    return filename

def create_car_data_old():
    """This creates timeseries where the charging processes are simultaneous for 1/3 of all 
    the vehicles. This creates critical demand points where the grid will be overloaded.

    Returns:
        str: Path of the created CSV with the timeseries
    """    
    # Electromobility
    car0 = "ZLE_freetime_W53_fdf22_Volkswagen_ID.3_2020_8fa92_avai_a7741.pickle"
    car1 = "ZLE_fulltime_W53_75853_Renault_Zoe_Q90_2019_9e477_avai_53e02.pickle"
    car2 = "ZLE_fulltime_W53_75853_Tesla_Model_3_Long_Range_AWD_2019_15218_avai_597fd.pickle"

    car0_path = join(DATA_CAR_DIR, "_old", car0)
    car1_path = join(DATA_CAR_DIR, "_old", car1)
    car2_path = join(DATA_CAR_DIR, "_old", car2)

    paths = [car0_path, car1_path, car2_path]

    data_car_kw = pd.DataFrame()

    for i, path in enumerate(paths):
        pickle_off = gzip.open(path, "rb")
        data = pickle.load(pickle_off)
        pickle_off.close()
        charg_point = data["profile"]["charging_point"]
        charg_cap = data["profile"]["charging_cap"]
        charg_soc = data["profile"]["soc"]
        total_load = [0]*len(charg_cap)
        for x in range(0, len(charg_cap)):
            if charg_point[x] == "home" and charg_soc[x] < 1:
                total_load[x] = charg_cap[x]
        data_car_kw[f"car_{i}"] = total_load[:24*4*366]  # charging

    for i in range(3, 147):
        j = i % 3
        data_car_kw[f"car_{i}"] = data_car_kw[f"car_{j}"]

    START = "2019-01-01 00:00:00"
    END = "2019-12-31 23:45:00"
    dti = pd.date_range(start=START, end=END, freq="15T")
    steps = len(dti)
    data_car_kw = data_car_kw.head(steps)
    data_car_kw["Time"] = dti
    data_car_kw.set_index("Time", inplace=True)
    data_car_w = data_car_kw*1000  # Car data needs to be in W
    filename = join(DATA_PROCESSED_DIR, "ev_loads_old.csv")
    data_car_w.to_csv(filename)
    logging.info(f"EV data saved to {filename}")
    return filename


if __name__ == '__main__':
    weather_filename= "Braunschweig_meteodata_2019.csv"
    weather_filename_10mins = resample_data(filename=weather_filename, path=DATA_RAW_DIR)

    household_data_filename= create_household_data()
    resample_data(filename=household_data_filename, path=DATA_PROCESSED_DIR)

    car_data_filename = create_car_data()
    resample_data(filename=car_data_filename, path=DATA_PROCESSED_DIR)

    car_data_filename_old = create_car_data_old()
    resample_data(filename=car_data_filename_old, path=DATA_PROCESSED_DIR)

    create_pv_data(meteo_data_path= weather_filename_10mins)
    create_grid()

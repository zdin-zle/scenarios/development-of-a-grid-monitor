'''
Created on 24.05.2022

@author: Fernando Penaherrera @UOL/OFFIS
'''
from copy import deepcopy
import pickle
import gzip
import time
import json
from os.path import join, isfile
import logging
from xml.dom import ValidationErr
import pandapower as pp
import pandas as pd
from tqdm import tqdm
import correction_methods
import math
from collections import Counter
import prepare_data

from commons import DATA_PROCESSED_DIR, RES_DATA_DIR, DATA_RAW_DIR, Methods

logging.basicConfig(level=logging.INFO)
logging.basicConfig(format='%(asctime)s %(message)s')


def prepare_simulation_data(overwrite=True):
    """Collection of methods for preparing the required simulation data

    Args:
        overwrite (bool, optional): Overwrite the current data files. Defaults to True.
    """    
    # Weather data resampling
    filename = join(DATA_PROCESSED_DIR,
                    "Braunschweig_meteodata_2019_10min.csv")
    if not isfile(filename) or overwrite:
        weather_filename = "Braunschweig_meteodata_2019.csv"
        weather_filename_10mins = prepare_data.resample_data(
            filename=weather_filename, path=DATA_RAW_DIR)
    else:
        weather_filename_10mins = filename
        logging.info(f"File exists: {filename}")

    # Grid JSON file
    filename = join(DATA_PROCESSED_DIR, "Grid_Model.json")
    if not isfile(filename) or overwrite:
        prepare_data.create_grid()
    else:
        logging.info(f"File exists: {filename}")

    # Hoousehold loads in 10min resolution
    filename = join(DATA_PROCESSED_DIR, "Household_Loads_10min.csv")
    if not isfile(filename) or overwrite:
        household_data_filename = prepare_data.create_household_data(
            houses=49, units_per_house=6, year=2019)
        prepare_data.resample_data(
            filename=household_data_filename, path=DATA_PROCESSED_DIR)
    else:
        logging.info(f"File exists: {filename}")

    # PV Production CSV
    filename = join(DATA_PROCESSED_DIR, "pv_production.csv")
    if not isfile(filename)  or overwrite:
        prepare_data.create_pv_data(meteo_data_path=weather_filename_10mins)
    else:
        logging.info(f"File exists: {filename}")

    # EV Loads (distributed randomly)
    filename = join(DATA_PROCESSED_DIR, "ev_loads_10min.csv")
    if not isfile(filename):
        car_data_filename = prepare_data.create_car_data()
        prepare_data.resample_data(
            filename=car_data_filename, path=DATA_PROCESSED_DIR)
    else:
        logging.info(f"File exists: {filename}")

    # EV Loads (critically stacked to produce grid overloads)
    filename = join(DATA_PROCESSED_DIR, "ev_loads_old_10min.csv")
    if not isfile(filename):
        car_data_filename_old = prepare_data.create_car_data_old()
        prepare_data.resample_data(
            filename=car_data_filename_old, path=DATA_PROCESSED_DIR)
    else:
        logging.info(f"File exists: {filename}")

def calculate_scenario(method=Methods.MAX_DEPEND, config="test", scenario="detail", EV="new"):
    """Calculates the scenario for the given parameters and configurations. This performs a quasi-dinamic simulation
    by advancing over the input data and performing power-flow analysis at each simulation step. When grid voltage levels violations
    are found, applies one of the corrected methods to attempt to eliminate these violations.

    Args:
        method (Method.item, optional): One of the values from commons.Methods. Defaults to Methods.MAX_DEPEND.
        config (str, optional): Configuration for running the simulation. One of ['run', 'test', 'reduced']. Defaults to 'test'.
        scenario (str, optional): Control Scenarios. 
            'detail.' provides a strict control for every violantion. 
            'statistic' allows a maximum number of weekly voltage violations Defaults to "detail".
        EV (str, optional): Model for EV distribution. "new" implements a distributed, more realistic distribution of EV in the grid. Defaults to "new".

    Raises:
        ValueError: If 'scenario' not in ['detail', 'weekly'] 
        ValueError: If 'config' not in ['run', 'test', 'reduced']
        ValueError: If 'EV' not in ["new", "old"]
    """
    logging.info(f"Working on scenario {method.name} - {scenario} control")
    if scenario not in ["weekly", "detail"]:
        raise ValueError("'scenario' must be either 'detail', 'weekly' ")

    if config not in ["test", "run", "reduced"]:
        raise ValueError(
            "Configuration parameter must be either 'run', 'test', 'reduced'")

    if EV not in ["new", "old"]:
        raise ValueError(
            "Configuration parameter must be either 'new' or 'old'")
    # Electromobility
    # Load Car Data
    if EV == "new":
        ev_filename = join(DATA_PROCESSED_DIR, "ev_loads_10min.csv")

    if EV == "old":
        ev_filename = join(DATA_PROCESSED_DIR, "ev_loads_old_10min.csv")

    data_car_w = pd.read_csv(ev_filename, index_col="Time", parse_dates=True)

    # Load Grid
    grid_file = join(DATA_PROCESSED_DIR, "Grid_Model.json")
    net = pp.from_json(grid_file)

    # Load Data
    households_file = join(DATA_PROCESSED_DIR, "Household_loads_10min.csv")
    pv_file = join(DATA_PROCESSED_DIR, "pv_production.csv")
    data_household_loads = pd.read_csv(
        households_file, index_col="Time", parse_dates=True)
    data_pv_production = pd.read_csv(
        pv_file, index_col="Time", parse_dates=True)

    # Assert that the simulation runs for the number of steps of the shortest dataframe to avoid errors.
    n_data = min(
        data_household_loads.shape[0],
        data_pv_production.shape[0],
        data_car_w.shape[0])
   
    # Battery Sizing
    battery_size = data_household_loads.sum()/4/1e6  # kWh
    battery_size = round(battery_size, 1)
    battery_size *= 1000

    if config == "test":
        battery_size *= 6  # Greatly exagerate the size for testing

    if config == "reduced":
        battery_size *= 0.5  # Reduce battery size for experimentation. This produces non convergences.


    battery_power_lims = list(battery_size)

    # Assign the battery limits in W to the Storage dict. This is a placeholder
    for idx in net.storage.index:
        net.storage.at[idx, "min_p_mw"] = -battery_power_lims[idx]
        net.storage.at[idx, "max_p_mw"] = battery_power_lims[idx]

    # Save voltage levels before and after the correction
    res_max_vm_pu_after = []
    res_min_vm_pu_after = []
    res_max_vm_pu_before = []
    res_min_vm_pu_before = []

    # Save Line Load Levels before and after the correction
    res_max_lines_before = []
    res_min_lines_before = []
    res_max_lines_after = []
    res_min_lines_after = []

    # Save Trafo Load Levels 
    res_trafo_load_before=[]
    res_trafo_load_after=[]
    # Save Storage requirements, iterations and convergence time
    res_sto_dict = {}

    if config == "run":
        ranges = range(n_data)
    elif config == "test":
        ranges = range(5858, 5858+144)
    elif config == "reduced":
        ranges = range(144*(15*7-1), 144*(15*7-1+7))


    '''
    Statistic control indicates the that
    • 95% of the 10 minute averages of the RMS mains voltage each week must be 
      within the voltage band of ± 10% of the nominal voltage 

      -> Here we take a reduced band of ± 5% to control in the low voltage side
    

    • 100% of the 10 minute averages of the rms value of the mains voltage for 
      each week must be within the voltage band of - 15% to + 10% of the nominal
      voltage 

      ->  Here we take a reduced band of ± 10% to control in the low voltage side

    DIN EN 50160
    '''
    # Two lists are created
    issues_record = []  # Saves only the issues between +-5%. Some of them can be allowed
    issues_crit_record = []  # Saves only the issues between +-10%. This cannot be allowed

    # Saves only the issues between +-5% AFTER correction. This may cointain 5% of True values
    issues_record_after = []
    # Saves only the issues between +-10% AFTER. This must be all False after correction.
    issues_crit_record_after = []

    
    for j in tqdm(ranges):
        # for j in ranges:
        # By default the grid status is correct
        issue = False
        crit_issue = False
        control = False
        if method== Methods.BASIC:
            for i in range(0, 49):
                net.load.at[i, "p_mw"] = data_household_loads[f"House{i}"][j]
                net.sgen.at[i, "p_mw"] = 0
                net.storage.at[i, "p_mw"] = 0
                net.storage.at[i, "q_mvar"] = 0

            # Assign 1 EV per lload, starting at 49
            
            k = 49
            while k < 195:
                net.load.at[k, "p_mw"] = 0
                k += 1
                    
        else:
            for i in range(0, 49):
                net.load.at[i, "p_mw"] = data_household_loads[f"House{i}"][j]
                net.sgen.at[i, "p_mw"] = data_pv_production[f"PV{i}"][j]
                net.storage.at[i, "p_mw"] = 0
                net.storage.at[i, "q_mvar"] = 0

            # Assign 1 EV per lload, starting at 49

            k = 49
            n_ev = 0
            while k < 195:
                net.load.at[k, "p_mw"] = data_car_w[f"car_{n_ev}"][j]
                k += 1
                n_ev+=1

        # This is only to see if withouth corrections there are problems in the buses
        pp.runpp(net)

        res_max_vm_pu_before.append(max(net.res_bus["vm_pu"]))
        res_min_vm_pu_before.append(min(net.res_bus["vm_pu"]))

        res_max_lines_before.append(max(net.res_line["loading_percent"]))
        res_min_lines_before.append(min(net.res_line["loading_percent"]))

        res_trafo_load_before.append(net.res_trafo.at[0,"loading_percent"])

        # This skips all of the statistical weekly analysis and corrects
        # if there is an issue or a critical issue
        if (min(net.res_bus["vm_pu"]) < 0.95 or max(net.res_bus["vm_pu"]) > 1.050):
            issue = True
            if (min(net.res_bus["vm_pu"]) < 0.90 or max(net.res_bus["vm_pu"]) > 1.10):
                crit_issue = True
                if config == "test":
                    logging.info(
                        f"Critical grid situation found at iteration {j}")

        issues_record.append(issue)
        issues_crit_record.append(crit_issue)
        issues_record_after.append(issue)
        issues_crit_record_after.append(crit_issue)

        if scenario == "detail":
            if issue:
                control = True

        elif scenario == "weekly":

            if len(issues_record_after) >= 24*6*7:
                # Takes the last week to control
                issues_survey_week = issues_record_after[-24*6*7:]
            else:
                # For the first week needs to be reduced
                issues_survey_week = deepcopy(issues_record_after)

            # allowed number of violations
            max_violations = int(
                math.ceil(len(issues_survey_week)*0.05))  # max 50 per week

            if config == "test":  # For testing the week I limit to 13 to see if it works.
                max_violations = min(max_violations, 13)

            counts = Counter(issues_survey_week)
            counts_true = counts[True]
            if counts_true >= max_violations:
                control = True
                logging.info("Max number of weekly violations reached.")
            if crit_issue:
                control = True

            # Notification dictionary for evaluation
            if config == "test":
                info_dict = {"Step": len(
                    issues_record_after), "Max Viol": max_violations, "Week Issues": counts_true}
                print(info_dict)

        if control:
            if config in ["test","reduced"] :
                print(f"Controlling the net at iteration {j}")
            time_start = time.time()
            # if method == Methods.VECTOR:
            #     net, iters = correction_methods.vector_method(
            #         net, restricted=False)
            if method == Methods.VECTOR_ACTIVE:
                net, iters = correction_methods.vector_method_improved(net)
            if method == Methods.VECTOR_REACTIVE:
                net, iters = correction_methods.vector_method(
                    net, restricted=True, reactive=True)
            if method == Methods.MAX_DEPEND:
                if config == "test":
                    net, iters = correction_methods.max_dependencies_method(
                        net, iterationSteps=3000)
                if config == "run":
                    net, iters = correction_methods.max_dependencies_method(
                        net, iterationSteps=500)
                if config == "reduced":
                    net, iters = correction_methods.max_dependencies_method(
                        net, iterationSteps=500)

            # Save the dict results in a dict of dicts
            storages_dict = {}
            storages_dict_Q = {}
            for sto_i in net.storage.index:
                if net.storage.at[sto_i, "p_mw"] != 0:
                    storages_dict[sto_i] = round(
                        net.storage.at[sto_i, "p_mw"], 2)
                    if method == Methods.VECTOR_REACTIVE:
                        storages_dict_Q[sto_i] = round(
                            net.storage.at[sto_i, "q_mvar"], 2)

            res_sto_dict[j] = {}
            res_sto_dict[j]["P"] = storages_dict
            res_sto_dict[j]["Q"] = storages_dict_Q
            res_sto_dict[j]["time"] = round(time.time()-time_start, 5)
            res_sto_dict[j]["its"] = iters

        res_max_vm_pu_after.append(max(net.res_bus["vm_pu"]))
        res_min_vm_pu_after.append(min(net.res_bus["vm_pu"]))

        res_max_lines_after.append(max(net.res_line["loading_percent"]))
        res_min_lines_after.append(min(net.res_line["loading_percent"]))

        res_trafo_load_after.append(net.res_trafo.at[0,"loading_percent"])
        # evaluate if there are issues remaining after control:
        issue_after = False
        crit_issue_after = False
        if (min(net.res_bus["vm_pu"]) < 0.95 or max(net.res_bus["vm_pu"]) > 1.050):
            issue_after = True

        if (min(net.res_bus["vm_pu"]) < 0.90 or max(net.res_bus["vm_pu"]) > 1.10):
            issue_after = True
            crit_issue_after = True

        # Since the last issue was probably resolved, remove it and replace it by the corrected version
        issues_record_after = issues_record_after[:-1]
        issues_record_after.append(issue_after)

        issues_crit_record_after = issues_crit_record_after[:-1]
        issues_crit_record_after.append(crit_issue_after)

    TEST_string = ""
    if config == "test":
        TEST_string = "test_"
    if config == "reduced":
        TEST_string = "reduced_"
    res_json_dir = join(
        RES_DATA_DIR, f"results_{TEST_string}{EV}_{scenario}_{method.name}.json")
    with open(res_json_dir, 'w') as fp:
        json.dump(res_sto_dict, fp,  indent=4)

    # Save results in a dataframe
    res_bus = pd.DataFrame()
    res_bus["Min vm_pu"] = res_min_vm_pu_before
    res_bus["Max vm_pu"] = res_max_vm_pu_before
    res_bus["Min vm_pu After"] = res_min_vm_pu_after
    res_bus["Max vm_pu After"] = res_max_vm_pu_after

    res_bus["Min Line Load"] = res_min_lines_before
    res_bus["Max Line Load"] = res_max_lines_before
    res_bus["Min Line Load After"] = res_min_lines_after
    res_bus["Max Line Load After"] = res_max_lines_after

    res_bus["Trafo Load Before"] = res_trafo_load_before
    res_bus["Trafo Load After"] = res_trafo_load_after

    res_bus["Issues Before"] = issues_record
    res_bus["Crit Issues Before"] = issues_crit_record

    res_bus["Issues After"] = issues_record_after
    res_bus["Crit Issues After"] = issues_crit_record_after

    START = "2019-01-01 00:00:00"
    END = "2019-12-31 23:50:00"
    dti_res = pd.date_range(start=START, end=END, freq="10T")

    if config == "run":
        res_bus["Date"] = dti_res[:n_data]
    elif config == "test":
        res_bus["Date"] = dti_res[5858:5858+144]
    elif config == "reduced":
        res_bus["Date"] = dti_res[144*(15*7-7):144*(15*7)]


    res_bus.set_index("Date", inplace=True)
    res_bus.to_csv(
        join(RES_DATA_DIR, f"extremes_{TEST_string}{EV}_{scenario}_{method.name}.csv"))


if __name__ == '__main__':

    #filename = join(DATA_PROCESSED_DIR, "ev_loads_10min.csv")

    prepare_simulation_data(overwrite=False)

    for method in [Methods.VECTOR_ACTIVE, Methods.VECTOR_REACTIVE, Methods.MAX_DEPEND]:
        #calculate_scenario(method, config="test", scenario="detail")
        #calculate_scenario(method, config="run", scenario="detail", EV="old")
        calculate_scenario(method, config="reduced", scenario="detail", EV="old")
        #calculate_scenario(method, config="run", scenario="detail")

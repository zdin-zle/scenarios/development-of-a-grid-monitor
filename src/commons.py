'''
Common parameters, such as folder names, correction methods, etc.
Created on 24.05.2022

@author: Fernando Penaherrera @UOL/OFFIS
'''

import os
from enum import Enum
from os.path import join


def get_project_root():
    """
    Returns the path to the project root directory.

    :return: A directory path.
    :rtype: str
    """
    return os.path.realpath(os.path.join(
        os.path.dirname(__file__),
        os.pardir,
    ))


BASE_DIR = get_project_root()
DATA_DIR = join(BASE_DIR, "data")
DATA_PROCESSED_DIR = join(DATA_DIR, "processed")
DATA_RAW_DIR = join(DATA_DIR, "raw")
DATA_CAR_DIR = join(DATA_DIR, "car")
RES_DIR = join(BASE_DIR, "results")
RES_DATA_DIR = join(RES_DIR, "data")
RES_POSTPROC_DIR = join(RES_DIR, "postprocessed")
SRC_DIR = join(BASE_DIR, "src")
TEST_DIR = join(BASE_DIR, "tests")


class Methods(Enum):
    '''
    List of the correction methods

    BASIC = Sets the inputs from the EVs, PVs and BSS to 0. "As is" grid configuration.
    VECTOR_IMPROVED = Correction based on the Real Power Vector [dP], with [dQ]=0
    VECTOR_REACTIVE = Correction based on the Real and Reactive Power Vectors [dP | dQ]
    MAX_DEPEND = Correction based on the Max Dependent element of the Jacobian Matrix
    '''
    BASIC = 0
    VECTOR_ACTIVE = 1
    VECTOR_REACTIVE = 2
    MAX_DEPEND = 3


if __name__ == '__main__':
    print(DATA_DIR)
    print(Methods.MAX_DEPEND.name)

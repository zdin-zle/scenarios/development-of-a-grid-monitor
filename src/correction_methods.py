'''
A collection of the correction methods for easy import/export

Created on 24.05.2022

@author: Fernando Penaherrera @UOL/OFFIS
'''
import numpy as np
import pandapower as pp


def vector_method(net, restricted=True, reactive=False, dynamic_cos_phi=False):
    """    Correction Method based on multiplication of a "Desired Voltage Vector" and the Jacobian Matrix.

    Args:
        net (pandapower.net): A pandapower net object with all the elements. Last bus must be slack.
        restricted (bool, optional): Apply Power Flow direction restrictions based on voltage levels.. Defaults to True.
        reactive (bool, optional): Use reactive power. Defaults to False.
        dynamic_cos_phi (bool, optional): Implement a variable cos_phi value instead of a fixed to 0.95. Defaults to False.

    Returns:
        pandapower.net: Pandapower.net object with the corrected voltage levels and power injection at the storage component
        int: Iterations counter
    """
    y = 1  # Iterations counter
    converged = False
    while not converged:
        jacobian = net._ppc["internal"]["J"].todense()
        n_buses = len(net.res_bus["vm_pu"])
        # The first section corresponds to "df", which it is set to 0 since changes in frequency are undesired
        list_v = [0]*(n_buses-1)

        e = 0.1/400
        for v in net.res_bus["vm_pu"][0:-1]:  # last  bus is always the slack
            if v < 0.95:
                dv = (0.95 + e - v)*400

            elif v > 1.05:
                dv = (1.05 - e - v)*400
            else:
                dv = 0
            list_v.append(dv)

        assert(len(list_v) == (n_buses-1)*2)

        deltas_mx = np.dot(jacobian, list_v)

        # Skip the last bus since it is the trafo LV side and I cannot control it
        deltas_P_list = [deltas_mx[0, i] for i in range(0, n_buses-2)]
        deltas_Q_list = [deltas_mx[0, i]
                         for i in range(n_buses-1, n_buses-1+n_buses-2)]
        assert(len(deltas_P_list) == n_buses-2)
        assert(len(deltas_Q_list) == n_buses-2)
        if restricted:
            '''
            There is have a small intra-trading for the grid. 
            I put a restriction that the power required agrees with the simbol of the delta
            P<0 if dv>0 (Reduction of load at the bus if the voltageg is low)
            P>0 if dv<0 (Increase load at the bus if voltage is high)
            '''

            for i in range(0, n_buses-2):
                if list_v[n_buses-1+i] >= 0 and deltas_P_list[i] < 0:
                    deltas_P_list[i] = 0
                if list_v[n_buses-1+i] <= 0 and deltas_P_list[i] > 0:
                    deltas_P_list[i] = 0

        for sto_i in net.storage.index:
            # This iterates on all the elements of the storage
            # here a limit can be implemented
            # Momentary the limit is stored in the max_p_mw value

            if net.storage.at[sto_i, "p_mw"]-deltas_P_list[sto_i] > net.storage.at[sto_i, "max_p_mw"]:
                net.storage.at[sto_i,
                               "p_mw"] = net.storage.at[sto_i, "max_p_mw"]

            elif net.storage.at[sto_i, "p_mw"]-deltas_P_list[sto_i] < net.storage.at[sto_i, "min_p_mw"]:
                net.storage.at[sto_i,
                               "p_mw"] = net.storage.at[sto_i, "min_p_mw"]
            else:
                net.storage.at[sto_i, "p_mw"] -= deltas_P_list[sto_i]*100

        ####
            '''
            The implementation of reactive power follows a limit in the phase angle
            The cos_phi is dependend on the relation between Max_P and actual P
            '''
            if reactive:
                cos_phi = 0.95

                p_sto = abs(net.storage.at[sto_i, "p_mw"])
                if dynamic_cos_phi: # Follows a linear increment on the cos_phi with the Active Power.
                    cos_phi = calc_cos_phi(
                        p_sto, net.storage.at[sto_i, "max_p_mw"])

                tan_phi = (1-cos_phi**2)**0.5/cos_phi

                if cos_phi == 1:
                    net.storage.at[sto_i, "q_mvar"] = 0
                else:
                    max_q = abs(p_sto)*tan_phi
                    net.storage.at[sto_i, "q_mvar"] -= deltas_Q_list[sto_i]*100
                    actual_q = net.storage.at[sto_i, "q_mvar"]
                    if abs(net.storage.at[sto_i, "q_mvar"]) > max_q and actual_q != 0:
                        net.storage.at[sto_i,
                                       "q_mvar"] = actual_q/abs(actual_q)*max_q

        pp.runpp(net)

        e = 0.1/400  # Tolerance for the convergence test
        if max(net.res_bus["vm_pu"]) <= 1.05 + e:
            if min(net.res_bus["vm_pu"]) >= 0.95 - e:
                converged = True
        # TODO A converge test that proofs if batteries limits have been reached to stop the simulation is required.
        
        y += 1
        if y >= 200:
            converged = True
            print(f"Not converged after {y} iterations")

    return net, y


def calc_cos_phi(p: float, p_max: float) -> float:
    """Calculates the cos_phi based on the relations between the real power and the maximum real power

    Args:
        p (float): Real Power, W
        p_max (float): Max Power, W

    Returns:
        float: Phase angle, cos_phi
    """

    if p/p_max < 0.5:
        cos_phi = 1
    else:
        # (cos_phi-0) /(0.95-0)=(p-p_max/2)/(p_max-p_max/2)
        cos_phi = 0.95*(p-p_max/2)/(p_max-p_max/2)
    return cos_phi


def vector_method_improved(net):
    """This method skips reactive power and considers the different submatrixes for calculating active power.

    Args:
        net (pandapower.net): A pandapower net

    Returns:
        pandapower.net: Pandapower.net object with the corrected voltage levels and power injection at the storage component
        int: Iterations counter
    """
    y = 1  # This is the counter for the iterations
    converged = False
    while not converged:
        jacobian = net._ppc["internal"]["J"].todense()
        n_buses = len(net.res_bus["vm_pu"])

        # Matrix of M rows and N columns:
        m_rows, n_cols, = jacobian.shape[0], jacobian.shape[1]
        m_rows_2, n_cols_2 = int(m_rows/2), int(n_cols/2)
        dP_df = jacobian[:m_rows_2, :n_cols_2]
        dP_dv = jacobian[:m_rows_2, n_cols_2:n_cols]
        dQ_df = jacobian[m_rows_2:m_rows, :n_cols_2]
        dQ_dv = jacobian[m_rows_2:m_rows, n_cols_2:n_cols]

        df_dQ = np.linalg.inv(dQ_df)

        delta_P_delta_v = dP_dv - np.matmul(dP_df, np.matmul(df_dQ, dQ_dv))

        list_v = []

        e = 0.1/400
        for v in net.res_bus["vm_pu"][0:-1]:  # last  bus is always the slack
            if v < 0.95:
                dv = (0.95+ e - v)*400
            elif v > 1.05:
                dv = (1.05- e - v)*400
            else:
                dv = 0
            list_v.append(dv)
        # The last voltage has to be 0 since it is the low voltage side and I cannot control it
        list_v[-1] = 0
        assert(len(list_v) == n_buses-1)

        deltas_mx = np.dot(delta_P_delta_v, list_v)

        # Skip the last bus since it is the trafo LV side and I cannot control it
        deltas_list = [deltas_mx[0, i] for i in range(0, n_buses-2)]

        '''
        I put a restriction that the power required agrees with the simbol of the delta
        P<0 if dv>0 (Reduction of load if the voltageg is low)
        P>0 if dv<0 (Increase load if voltage is high)
        '''

        for i in range(0, n_buses-2):
            if list_v[i] >= 0 and deltas_list[i] < 0:
                deltas_list[i] = 0
            if list_v[i] <= 0 and deltas_list[i] > 0:
                deltas_list[i] = 0

        for sto_i in net.storage.index:
            # This iterates on all the elements of the storage
            # here a limit can be implemented

            if net.storage.at[sto_i, "p_mw"]-deltas_list[sto_i] > net.storage.at[sto_i, "max_p_mw"]:
                net.storage.at[sto_i,
                               "p_mw"] = net.storage.at[sto_i, "max_p_mw"]

            elif net.storage.at[sto_i, "p_mw"]-deltas_list[sto_i] < net.storage.at[sto_i, "min_p_mw"]:
                net.storage.at[sto_i,
                               "p_mw"] = net.storage.at[sto_i, "min_p_mw"]
            else:
                # for faster convergence
                net.storage.at[sto_i, "p_mw"] -= deltas_list[sto_i]*100

        # print(res_dict)
        pp.runpp(net)

        # 0.01/400
        e = 0.1/400
        if max(net.res_bus["vm_pu"]) <= 1.05 + e:
            if min(net.res_bus["vm_pu"]) >= 0.95 - e:
                converged = True

        y += 1
        
        # bus_res[y]=net.res_bus["vm_pu"]
        if y >= 200:
            converged = True
            print(f"Not converged after {y} iterations")

    return net, y


def max_dependencies_method(net, iterationSteps=500):
    """Correction method based on the element of the matrix with the higher dependencies on the Power/Voltage relation
    Looks for the element of the inverted Jacobian Matrix with produces the highes changes in V_n given a P_n value.

    Args:
        net (pandapower.net): A pandapower net
        iterationSteps (int, optional): Size of the gradual increment in Power. Defaults to 500.

    Returns:
        pandapower.net: Pandapower.net object with the corrected voltage levels and power injection at the storage component
        int: Iterations counter
    """
    y = 1
    while True:

        errorlist = []

        for i in range(len(net.bus)):
            if net.res_bus.vm_pu.at[i] > 1.05:
                errorlist.append(net.res_bus.vm_pu.at[i] - 1.05)
            elif net.res_bus.vm_pu.at[i] < 0.95:
                errorlist.append(0.95 - net.res_bus.vm_pu.at[i])
            else:
                errorlist.append(0)

        max_value = max(errorlist)
        max_index = errorlist.index(max_value)

        if max_value > 0.0:

            # This loop will be run through until the largest violation is corrected
            while True:

                # get Jacobi Matrix J
                jacobi = net._ppc["internal"]["J"].todense()

                # invert J-Matrix
                invjacobi = np.linalg.inv(jacobi)

                # select sub matrix (for dP)
                l, b = invjacobi.shape
                dP_matrix = invjacobi[int(l/2): l, : int(b/2)]

                # select row in sub matrix with voltage range violation
                ppc_index = net._pd2ppc_lookups["bus"][max_index]

                '''
                translate to ppc_index (this grid will have the same indices, 
                other structures could have different ppc indices)
                When a power flow is carried out, the element based grid model 
                is translated into a bus-branch model.
                That bus-branch model is stored in a data structure that is
                based on the PYPOWER/MATPOWER casefile (with some extensions).
                This ppc can be accesed after power flow
                '''

                violationmatrix = dP_matrix[ppc_index:ppc_index+1, :]

                # This While loop will be run through until a bus is found where it is possible to feed in.
                while True:
                    # find maximum of violation matrix
                    max_jacobi = violationmatrix.max()
                    max_jacobi_index = np.where(violationmatrix == max_jacobi)
                    dP_bus = net._pd2ppc_lookups["bus"][max_jacobi_index[1]][0]
                    # check if the bus has a storage component
                    newnet = net.storage.set_index("bus")
                    try:
                        data = newnet.loc[dP_bus]
                        # print(data.q_mvar)
                        does_exists = True
                    except:
                        does_exists = False

                    if does_exists == True and net.storage.at[dP_bus, 'in_service'] == True:

                        # check if it is possible to feed in more P or Q
                        # following if statement could be removed
                        if net.res_bus.vm_pu.at[max_index] < 0.95 and float(data.p_mw) > net.storage.at[dP_bus, 'min_p_mw']:
                            net.storage.at[dP_bus, 'p_mw'] = net.storage.at[dP_bus,
                                                                            'p_mw'] - iterationSteps
                            break
                        elif net.res_bus.vm_pu.at[max_index] > 1.05 and float(data.p_mw) < net.storage.at[dP_bus, 'max_p_mw']:
                            net.storage.at[dP_bus, 'p_mw'] = net.storage.at[dP_bus,
                                                                            'p_mw'] + iterationSteps
                            break
                        else:
                            violationmatrix[max_jacobi_index] = 0
                    else:
                        violationmatrix[max_jacobi_index] = 0

                pp.runpp(net)
                y += 1
                if y == 10000:
                    print(f"Not converged after {y} iterations")
                    break

                if net.res_bus.vm_pu.at[max_index] < 1.05 or net.res_bus.vm_pu.at[max_index] > 0.95:
                    break
        else:
            break
        pp.runpp(net)
        y += 1

    return net, y

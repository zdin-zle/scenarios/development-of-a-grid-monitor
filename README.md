# Development of a Grid Observer

This project is created for the development and implementation of a Grid Observer for evaluation of a district grid and evaluation of grid status for assessment of corrective measurements to reduce or supres voltage violations.

## Installation instructions
1. Create a virtual environment, for example

    `conda create -n observer python=3.8`

2. Activate the environment

    `conda activate observer`


3. Download or clone this repository

4. Change the folder to the downloaded repository

    `cd PATH_TO_FOLDER`

5. From the cloned path, install the requirements file

    `pip install -r requirements.txt`

6. Run the main file under the "\src" folder

    `python src\main.py`

7. The postprocessing of data is done interactively in the Jupyter Notefook found under

    `docs\Notebooks\results_eval.ipynb`


## Model conception
The model follows the following conceptual design for input and outputs of parameters and grid voltage level corrections
|<img src="docs/SFa_Paper.png" >|
|:--:|
| **Figure 1:** Conceptual Model|


## Methods for Voltage Correction
Three correction methods are present: 
1. **Maximum Dependencies** method, 
which is based on the analysis of the voltage sensitivities at buses with voltage range violations. The method controls the feed in/out of active or reactive power at all grid buses with the help of the inverse Jacobian matrix.
2. **Vector - Active** method, 
which is based on the correction of the buses with voltage range violations using a vector representing the magnitude of said violations and the calculation of the required active power injection using the sensitivities found in the Jacobian Matrix
3. **Vector - Reactive** method,
is a similar to previous method, but with inclusion of reactive power injection. 

## Results on Correction
All of the correction methods provide accurate voltage control via power injection/absortion by batteries for the developed scenarios. The results provide the amount of power to be injected/absorded. This information is to be sent to an Energy Management System (centralized or decentralized) that can decide which actions to take (e.g., charge/discharge batteries of EVs, BSS, postpone charging, power curtailment in PVs, etc.)

|<img src="results/postprocessed/EV_new_vm_pu.png"  width="320" height="240">|
|:--:|
| **Figure 2:** Voltage Correction|

When reduced flexibilites exists (for example, smaller battery charge/discharge capacity), the Vector-Based methods may not converge due to their formulation.

|<img src="results/postprocessed/EV_reduced_old_vm_pu.png"  width="320" height="240">|
|:--:|
| **Figure 3:** Voltage Correction|

The calculations affect the whole grid bringing solving the voltage violation issues in all buses

|<img src="results/postprocessed/new_before.png"  width="320" height="240">|
|:--:|
| **Figure 4:** Grid Status Before Correction|


|<img src="results/postprocessed/new_after.png"  width="320" height="240">|
|:--:|
| **Figure 5:** Grid Status After Correction|


## Results on Calculation Time
The **Maximum Dependencies** method resulted in more stable and reliable calculations. The differences on calculation time shall also be consideredn when deciding which control method to implementet in an Energy Management System, since delays in communication can scalate in critical grid issues.
|<img src="results/postprocessed/EV_new_calc_time.png"  width="320" height="240">|
|:--:|
| **Figure 6:** Calculation Times|

 
 ## Authors
* Lead author and scientific conceptualization: Sarah Fayed @ University of Applied Sciences Emden/Leer
* Grid modelling and implementation: Fernando Penaherrera @ OFFIS Institute for Information Technology
* Battery and EV modelling and dimensioning: Henrik Wagner @ Technische Universität Braunschweig, elenia 


# Aknowlegments
Funded by the Lower Saxony Ministry of Science and Culture under grant number 11-76251-13-3/19 – ZN3488 within the Lower Saxony “Vorab” of the Volkswagen Foundation and supported by the Center for Digital Innovations (ZDIN).
